# WordToSQL

#### 项目介绍
本软件用于将指定格式的数据库设计文档， **生成mysql的建库，建表脚本；数据库正向工程** 。

#### 软件架构
Java Swing、POI


#### 安装教程

设置为可自行jar，或者在cmd中 jar命令。

#### 使用说明

1、主界面

![输入图片说明](https://images.gitee.com/uploads/images/2018/1127/132455_798949cb_395010.png "屏幕截图.png")

2、先点右上角“模板下载”
![输入图片说明](https://images.gitee.com/uploads/images/2018/1127/140402_3ff89047_395010.png "微信图片_20181127140251.png")

3、模板文件中提供基本的《数据库设计》文档格式，以及后面的数据库表结构设计模板：

![输入图片说明](https://images.gitee.com/uploads/images/2018/1127/140722_4234c1ca_395010.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1127/140711_910f5caa_395010.png "2.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/1127/140929_7bd97695_395010.png "屏幕截图.png")

4、生成的脚本
![输入图片说明](https://images.gitee.com/uploads/images/2018/1127/141018_886b7fbd_395010.png "屏幕截图.png")


在这里推荐一款 **生成数据库文档(逆向工程)** 的软件：
链接：https://pan.baidu.com/s/1u1-wIsVwtNoqNUbCbDauTg 
提取码：luh7 

![输入图片说明](https://images.gitee.com/uploads/images/2018/1127/141443_594debf0_395010.png "屏幕截图.png")